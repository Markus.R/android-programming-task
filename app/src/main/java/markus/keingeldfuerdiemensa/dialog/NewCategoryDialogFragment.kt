package markus.keingeldfuerdiemensa.dialog


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.utils.ColorTemplate.rgb
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentNewCategoryBinding

/**
 * DialogFragment um eine neue Kategorie zu erstellen
 */
class NewCategoryDialogFragment : DialogFragment(){
    private val TAG = NewCategoryDialogFragment::class.java.simpleName

    /**
     * wird aufgerufen, wenn das Layout geladen werden soll, und lädt es basierend auf dem [FragmentNewCategoryBinding],
     * verbindet darüber hinaus auch Klick- und Änderungs-Handler an die Buttons und Eingabefelder um ein Bestätigen des Dialoges ohne Eingabe zu verhindern
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DataBindingUtil.inflate<FragmentNewCategoryBinding>(LayoutInflater.from(activity), R.layout.fragment_new_category, null ,false)

        try {
            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)

            binding.nameInput.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(editable: Editable?) {
                    if (editable is Editable){
                        binding.buttonCreate.isEnabled = !editable.isBlank()
                    }
                }
            })

            binding.buttonCreate.isEnabled = false
            binding.buttonCreate.setOnClickListener {
                var color: String? = binding.colorInput.text.toString()
                if (!color!!.contains("#")) color = "#$color"
                if(color.length < 6) color = null
                if (color == null) {
                    categoryViewModel.addCategory(Category(name = binding.nameInput.text.toString()))
                } else {
                    categoryViewModel.addCategory(Category(name = binding.nameInput.text.toString(), color = rgb(color)))
                }
                dismiss()
            }

            binding.buttonCancel.setOnClickListener {
                dismiss()
            }
        } catch (e: Exception) { }

        return binding.root
    }

    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance() : NewCategoryDialogFragment {
            return NewCategoryDialogFragment()
        }
    }
}