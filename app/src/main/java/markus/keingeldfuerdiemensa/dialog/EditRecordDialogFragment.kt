package markus.keingeldfuerdiemensa.dialog


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import kotlinx.android.synthetic.main.fragment_new_record.*
import markus.keingeldfuerdiemensa.CategoryFragment
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.backend.MoneyRecord
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentEditRecordBinding
import markus.keingeldfuerdiemensa.databinding.FragmentNewRecordBinding
import markus.keingeldfuerdiemensa.uicomponent.CategoryChipFragment
import java.util.*


/**
 * DialogFragment um einen neuen Eintrag zu erstellen
 */
class EditRecordDialogFragment : DialogFragment(){
    private val TAG = EditRecordDialogFragment::class.java.simpleName
    private var baseRecord: MoneyRecord? = null
    var changedDate: Calendar? = null

    /**
     * wird aufgerufen, wenn das Layout geladen werden soll, und lädt es basierend auf dem [FragmentNewRecordBinding],
     * verbindet darüber hinaus auch Klick- und Änderungs-Handler an die Buttons und Eingabefelder um ein Bestätigen des Dialoges ohne Eingabe zu verhindern
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DataBindingUtil.inflate<FragmentEditRecordBinding>(LayoutInflater.from(activity),
                R.layout.fragment_edit_record, null ,false)
        try {
            val tx = childFragmentManager.beginTransaction()
            val fragment = CategoryChipFragment.newInstance(true)
            tx.replace(R.id.fragmentHolder, fragment)
            tx.commit()


            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)
            val moneyRecordViewModel = ViewModelProviders.of(activity!!).get(MoneyRecordViewModel::class.java)

            binding.record = baseRecord
            //update selected category
            categoryViewModel.setSelected(baseRecord?.category)


            binding.buttonChangeDateTime.setOnClickListener {
                if(baseRecord != null) showDateTimePicker(baseRecord!!.creationTimestamp)
            }


            categoryViewModel.selected.observe(this, Observer { //update create button to be only visible if a category got selected
                binding.buttonCreate.isEnabled = it is Category
            })

            //add euro sign to EditText, this has to be done programmatically because of older android versions
            val euroDrawable = VectorDrawableCompat.create(context!!.resources, R.drawable.ic_euro_symbol_black_24dp, null)
            binding.amountInput.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, euroDrawable, null)

            binding.buttonCreate.setOnClickListener {
                val checkedRadioButton = radioGroup.checkedRadioButtonId
                val amount = if(amountInput.text.toString().equals("")) 0f else amountInput.text.toString().toFloat()

                val signedAmount = if (checkedRadioButton == radioButtonOutgoing.id) -amount else amount

                val selectedCategory = categoryViewModel.getSelected()
                if (selectedCategory is Category && baseRecord is MoneyRecord) {
                    if (changedDate != null){
                        baseRecord!!.creationTimestamp = changedDate!!.time
                    }
                    //please kill me
                    baseRecord!!.categoryId = selectedCategory._cid
                    baseRecord!!.category = selectedCategory

                    baseRecord!!.amount = signedAmount
                    baseRecord!!.additionalInfo = descriptionInput.text.toString()

                    moneyRecordViewModel.updateMoneyRecord(baseRecord!!)

                    dismiss()
                }
            }

            binding.buttonCreate.isEnabled = false

            binding.buttonCancel.setOnClickListener {
                dismiss()
            }
        } catch (e: Exception) { }

        return binding.root
    }

    fun showDateTimePicker(recordDate: Date) {
        val currentDate = Calendar.getInstance()
        currentDate.time = recordDate
        changedDate = Calendar.getInstance()
        DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            changedDate?.set(year, monthOfYear, dayOfMonth)

            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                changedDate?.set(Calendar.HOUR_OF_DAY, hourOfDay)
                changedDate?.set(Calendar.MINUTE, minute)
                Log.v(TAG, "The choosen one " + changedDate?.getTime())
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show()
    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance(record: MoneyRecord? = null) : EditRecordDialogFragment {
            return EditRecordDialogFragment().apply{
                this.baseRecord = record
            }
        }
    }
}