package markus.keingeldfuerdiemensa.dialog

import android.annotation.TargetApi
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentCategorySelectBinding
import markus.keingeldfuerdiemensa.databinding.FragmentNewCategoryBinding

/**
 * DialogFragment für eine Liste an Kategorien mit Checkboxen daneben
 */
class CategorySelectDialogFragment : DialogFragment(){
    private val TAG = CategorySelectDialogFragment::class.java.simpleName

    /**
     * wird aufgerufen, wenn das Layout geladen werden soll, und lädt es basierend auf dem [FragmentNewCategoryBinding],
     * verbindet darüber hinaus auch Klick- und Änderungs-Handler an die Buttons und Eingabefelder um ein Bestätigen des Dialoges ohne Eingabe zu verhindern
     */

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DataBindingUtil.inflate<FragmentCategorySelectBinding>(LayoutInflater.from(activity), R.layout.fragment_category_select, null ,false)

        val changedCategories = HashMap<Category, Boolean>()
        try {
            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)

            categoryViewModel.categories.observe(this, Observer { //update create button to be only visible if a category got selected
                binding.checkboxHolder.removeAllViews()
                if (it is List<Category>) {
                    for (category in it.listIterator()){

                        binding.checkboxHolder.addView(CheckBox(context).apply {
                            text = category.name
                            isChecked = category.isEnabled
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                buttonTintList = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_enabled)), intArrayOf(category.color))
                            }

                            setOnClickListener {
                                changedCategories[category] = isChecked
                            }
                        })
                    }
                }
            })


            binding.buttonCreate.setOnClickListener {
                //save changed categories
                for (hm in changedCategories){
                    hm.key.isEnabled = hm.value
                    categoryViewModel.updateCategory(hm.key)
                }
                dismiss()
            }

        } catch (e: Exception) { }

        return binding.root
    }

    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance() : CategorySelectDialogFragment {
            return CategorySelectDialogFragment()
        }
    }
}