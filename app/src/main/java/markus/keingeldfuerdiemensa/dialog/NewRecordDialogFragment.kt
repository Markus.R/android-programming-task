package markus.keingeldfuerdiemensa.dialog


import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import kotlinx.android.synthetic.main.fragment_new_record.*
import markus.keingeldfuerdiemensa.CategoryFragment
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.backend.MoneyRecord
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentNewRecordBinding
import markus.keingeldfuerdiemensa.uicomponent.CategoryChipFragment
import markus.keingeldfuerdiemensa.uicomponent.ColorSelectFragment

/**
 * DialogFragment um einen neuen Eintrag zu erstellen
 */
class NewRecordDialogFragment : DialogFragment(){
    private val TAG = NewRecordDialogFragment::class.java.simpleName

    /**
     * wird aufgerufen, wenn das Layout geladen werden soll, und lädt es basierend auf dem [FragmentNewRecordBinding],
     * verbindet darüber hinaus auch Klick- und Änderungs-Handler an die Buttons und Eingabefelder um ein Bestätigen des Dialoges ohne Eingabe zu verhindern
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DataBindingUtil.inflate<FragmentNewRecordBinding>(LayoutInflater.from(activity),
                R.layout.fragment_new_record, null ,false)
        try {
            val tx = childFragmentManager.beginTransaction()
            val fragment = CategoryChipFragment.newInstance(true)
            tx.replace(R.id.fragmentHolder, fragment)
            tx.commit()

            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)
            val moneyRecordViewModel = ViewModelProviders.of(activity!!).get(MoneyRecordViewModel::class.java)

            categoryViewModel.selected.observe(this, Observer { //update create button to be only visible if a category got selected
                binding.buttonCreate.isEnabled = it is Category
            })

            //add euro sign to EditText, this has to be done programmatically because of older android versions
            val euroDrawable = VectorDrawableCompat.create(context!!.resources, R.drawable.ic_euro_symbol_black_24dp, null)
            //euroDrawable?.setColorFilter(resources.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN)
            binding.amountInput.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, euroDrawable, null)

            binding.buttonCreate.setOnClickListener {
                val checkedRadioButton = radioGroup.checkedRadioButtonId
                val amount = if(amountInput.text.toString().equals("")) 0f else amountInput.text.toString().toFloat()
                val signedAmount = if (checkedRadioButton == radioButtonOutgoing.id) -amount else amount
                val info = descriptionInput.text.toString()

                val selectedCategory = categoryViewModel.getSelected()
                if (selectedCategory is Category) {
                    moneyRecordViewModel.addMoneyRecord(MoneyRecord(amount = signedAmount, categoryId = selectedCategory._cid, category = selectedCategory, additionalInfo = info))
                    dismiss()
                }
            }

            binding.buttonCreate.isEnabled = false

            binding.buttonCancel.setOnClickListener {
                dismiss()
            }
        } catch (e: Exception) { }

        return binding.root
    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance() : NewRecordDialogFragment {
            return NewRecordDialogFragment()
        }
    }
}