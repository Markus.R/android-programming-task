package markus.keingeldfuerdiemensa.dialog


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.utils.ColorTemplate
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentEditCategoryBinding
import markus.keingeldfuerdiemensa.databinding.FragmentNewRecordBinding
import java.util.*


/**
 * DialogFragment um einen neuen Eintrag zu erstellen
 */
class EditCategoryDialogFragment : DialogFragment(){
    private val TAG = EditCategoryDialogFragment::class.java.simpleName
    private var baseCategory: Category? = null
    var changedDate: Calendar? = null

    /**
     * wird aufgerufen, wenn das Layout geladen werden soll, und lädt es basierend auf dem [FragmentNewRecordBinding],
     * verbindet darüber hinaus auch Klick- und Änderungs-Handler an die Buttons und Eingabefelder um ein Bestätigen des Dialoges ohne Eingabe zu verhindern
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DataBindingUtil.inflate<FragmentEditCategoryBinding>(LayoutInflater.from(activity),
                R.layout.fragment_edit_category, null ,false)
        try {
            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)

            binding.category = baseCategory

            binding.nameInput.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(editable: Editable?) {
                    if (editable is Editable){
                        binding.buttonCreate.isEnabled = !editable.isBlank()
                    }
                }
            })

            binding.buttonCreate.isEnabled = false
            binding.buttonCreate.setOnClickListener {
                var color: String? = binding.colorInput.text.toString()
                if (!color!!.contains("#")) color = "#$color"
                if(color.length < 6) color = null

                if (color != null) {
                    baseCategory!!.color = ColorTemplate.rgb(color)
                }

                baseCategory!!.name = binding.nameInput.text.toString()

                categoryViewModel.updateCategory(baseCategory!!)

                dismiss()
            }

            binding.buttonCancel.setOnClickListener {
                dismiss()
            }
        } catch (e: Exception) { }
        return binding.root
    }

    fun showDateTimePicker(recordDate: Date) {
        val currentDate = Calendar.getInstance()
        currentDate.time = recordDate
        changedDate = Calendar.getInstance()
        DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            changedDate?.set(year, monthOfYear, dayOfMonth)

            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                changedDate?.set(Calendar.HOUR_OF_DAY, hourOfDay)
                changedDate?.set(Calendar.MINUTE, minute)
                Log.v(TAG, "The choosen one " + changedDate?.getTime())
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show()
    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance(category: Category? = null) : EditCategoryDialogFragment {
            return EditCategoryDialogFragment().apply{
                this.baseCategory = category
            }
        }
    }
}