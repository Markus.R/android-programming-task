package markus.keingeldfuerdiemensa

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import markus.keingeldfuerdiemensa.backend.AppUtils
import markus.keingeldfuerdiemensa.backend.CategorySumTuple
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentDataAnalyzeBinding



class DataAnalyzeFragment : Fragment() {

    private val ANIM_TIME = 500 // 200ms steps to load charts -> smoother first load

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentDataAnalyzeBinding = DataBindingUtil.inflate(inflater!!, R.layout.fragment_data_analyze, container, false)

        val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)
        val moneyRecordViewModel = ViewModelProviders.of(activity!!).get(MoneyRecordViewModel::class.java)

        //overview card
        Handler().postDelayed({ //delay because the card has to load first
            moneyRecordViewModel.dateUntil.observe(this, Observer {
                moneyRecordViewModel.sumByCategory.observe(this, Observer {
                    if (it is List<CategorySumTuple>) {
                        val colors = IntArray(it.size)
                        val entries: List<PieEntry> = List(it.size) { i ->
                            colors[i] = it[i].category.color
                            return@List PieEntry(it[i].sum, AppUtils.toCurrencyString(it[i].sum))
                        }
                        val dataSet = PieDataSet(entries, "").apply {
                            this.colors = colors.toList()
                            valueTextSize = 0f
                        }
                        binding.pieChartOverview.apply {
                            legend.isEnabled = false
                            data = PieData(dataSet)

                            animateY(ANIM_TIME, Easing.EasingOption.EaseInOutCubic)

                            setHoleColor(Color.TRANSPARENT)
                        }
                        binding.pieChartOverview.description = Description().apply {
                            text = ""
                        }
                        binding.pieChartOverview.invalidate()
                    }

                })
            })

            moneyRecordViewModel.dateUntilHistory.observe(this, Observer {
                moneyRecordViewModel.historySumByCategory.observe(this, Observer {
                    Handler().postDelayed({
                        if (it is List<CategorySumTuple>) {
                            Log.w("TEST", "history list")
                            val colors = IntArray(it.size)
                            val entries: List<PieEntry> = List(it.size) { i ->
                                colors[i] = it[i].category.color
                                return@List PieEntry(it[i].sum, AppUtils.toCurrencyString(it[i].sum))
                            }
                            val dataSet = PieDataSet(entries, "").apply {
                                this.colors = colors.toList()
                                valueTextSize = 0f
                            }
                            binding.pieChartHistoryOverview.apply {
                                legend.isEnabled = false
                                data = PieData(dataSet)
                                animateY(ANIM_TIME, Easing.EasingOption.EaseInOutCubic)

                                setHoleColor(Color.TRANSPARENT)
                            }
                            binding.pieChartHistoryOverview.description = Description().apply {
                                text = ""
                            }
                            binding.pieChartHistoryOverview.invalidate()
                        }
                    }, ANIM_TIME * 1L)
                })
            })
        }, 300)
        return binding.root
    }

    companion object {
        fun newInstance() = DataAnalyzeFragment()
    }
}
