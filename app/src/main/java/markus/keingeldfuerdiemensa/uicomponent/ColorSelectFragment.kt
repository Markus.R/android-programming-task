package markus.keingeldfuerdiemensa.uicomponent

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.ColorUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.Chip
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.AppUtils.PRESET_COLORS
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentColorSelectBinding

class ColorSelectFragment: Fragment(){

    private lateinit var viewModel: CategoryViewModel
    private var selectMode: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentColorSelectBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_color_select, null ,false)


        val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)

        PRESET_COLORS.forEach {
            binding.colorViewHolder.addView(Chip(context, null, R.style.Widget_MaterialComponents_Chip_Choice).apply {
                text = "tt"
                isCheckable = true
                isClickable = true
                isCheckedIconVisible = false
                chipStrokeWidth = 6f

                chipStrokeColor = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_checked)), intArrayOf(it))
               // chipBackgroundColor = ColorStateList(arrayOf(intArrayOf(-android.R.attr.state_selected, android.R.attr.state_selected)), intArrayOf(Color.TRANSPARENT, it))

            })
        }


        /*categoryViewModel.categories.observe(this, Observer { //update create button to be only visible if a category got selected
            binding.chipGroup.removeAllViews()
            if (it is List<Category>) {
                it.forEach {
                    val category = it

                    binding.chipGroup.addView(Chip(context, null).apply {
                        text = it.name
                        isCheckable = true
                        isCheckedIconVisible = false
                        setChipStrokeWidthResource(R.dimen.mtrl_btn_stroke_size)

                        chipStrokeColor = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_selected)), intArrayOf(it.color))
                        chipBackgroundColor = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_selected)), intArrayOf(ColorUtils.setAlphaComponent(it.color, 20)))
                        isChecked = it.isEnabled
                        setOnClickListener {
                            category.isEnabled = isChecked
                            categoryViewModel.updateCategory(category)

                        }
                    })
                }
            }
        })*/

        return binding.root

    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        //maybe add a bundle of arguments here, so keep the option
        fun newInstance(selectMode: Boolean = false): ColorSelectFragment {
            val frag = ColorSelectFragment()
            frag.selectMode = selectMode
            return frag
        }
    }
}