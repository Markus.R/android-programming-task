package markus.keingeldfuerdiemensa.uicomponent

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.core.graphics.ColorUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.utils.ColorTemplate.rgb
import com.google.android.material.chip.Chip
import markus.keingeldfuerdiemensa.CategoryAdapter
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentCategoryChipBinding
import markus.keingeldfuerdiemensa.databinding.FragmentCategoryListBinding
import markus.keingeldfuerdiemensa.databinding.FragmentCategorySelectBinding

class CategoryChipFragment: Fragment(){

    private lateinit var viewModel: CategoryViewModel
    private var selectMode: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentCategoryChipBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_chip, null ,false)
        val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)

        categoryViewModel.categories.observe(this, Observer { //update create button to be only visible if a category got selected
            binding.chipGroup.removeAllViews()
            if (it is List<Category>) {

                val backgroundColor = ColorUtils.setAlphaComponent(rgb("#aaaaaa"), 20)
                val states = arrayOf(intArrayOf(android.R.attr.state_checked), intArrayOf(-android.R.attr.state_checked))

                it.forEach {
                    val category = it

                    binding.chipGroup.addView(Chip(context, null, R.style.Widget_MaterialComponents_Chip_Choice).apply {
                        text = it.name
                        isCheckable = true
                        isChecked = if(selectMode) categoryViewModel.getSelected() == category else it.isEnabled

                        isClickable = true
                        isCheckedIconVisible = false
                        setChipStrokeWidthResource(R.dimen.mtrl_btn_stroke_size)
                        chipBackgroundColor = ColorStateList(states, intArrayOf(ColorUtils.setAlphaComponent(category.color, 50), backgroundColor))
                        chipStrokeColor = ColorStateList(states, intArrayOf(category.color, Color.TRANSPARENT))
                        setOnCheckedChangeListener { compoundButton: CompoundButton, selected: Boolean ->
                            if (selectMode && selected) {
                                categoryViewModel.setSelected(category)
                            }else if(!selectMode){
                                category.isEnabled = selected
                                categoryViewModel.updateCategory(category)
                            }
                        }
                    })
                }
                binding.chipGroup.setOnCheckedChangeListener { chipGroup, i ->
                    if (i == -1){
                        categoryViewModel.setSelected(null)
                    }
                }
            }
        })
        return binding.root
    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        //maybe add a bundle of arguments here, so keep the option
        fun newInstance(selectMode: Boolean = false): CategoryChipFragment {
            val frag = CategoryChipFragment()
            frag.selectMode = selectMode
            return frag
        }
    }
}