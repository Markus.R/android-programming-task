package markus.keingeldfuerdiemensa.backend

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import java.util.*


/**
 * Datenbank-Klasse, die von der [RoomDatabase] erbt und Room eine Liste von DAOs bereitstellt
 */
@Database(entities = [MoneyRecord::class, Category::class], version = 13, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class MoneyDatabase : RoomDatabase() {

    /**
     * wird benutzt um eine einzelne Instanz der Datenbank zurückzugeben ohne einen statischen Kontext zu verwenden (Singleton)
     */
    companion object {
        private lateinit var INSTANCE : MoneyDatabase
        private var initialized = false
        /**
         * gibt die Instanz der Datenbank zurück und erstellt eine neue Instanz, falls es noch keine gibt
         * @param context der App-Kontext
         * @return Instanz der Datenbank
         */
        fun getInstance(context: Context) : MoneyDatabase{
            if (!initialized){ //ref to bug https://youtrack.jetbrains.com/issue/KT-21862
                INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        MoneyDatabase::class.java,
                        "MoneyDatabase")
                        .fallbackToDestructiveMigration()
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                //add a default category
                                doAsync {
                                    getInstance(context).categoryDao().insert(Category(name = "Allgemein"))
                                }.execute()
                            }
                        })
                        .build()
                initialized = true
            }
            return INSTANCE
        }
    }

    /**
     * wird von Room implementiert und gibt das DAO der MoneyRecords zurück
     * @return MoneyRecordDAO Objekt
     */
    abstract fun moneyRecordDao() : MoneyRecordDAO
    /**
     * wird von Room implementiert und gibt das DAO der Categories zurück
     * @return CategoryDAO Objekt
     */
    abstract fun categoryDao() : CategoryDAO
}


/**
 * Room-spezifische Klasse mit Methoden, die den Datentyp [Date] zu [Long] umwandeln (und umgekehrt), damit [Date] in einer SQLite Datenbank gespeichert werden kann
 */
class DateTypeConverter{

    /**
     * konvertiert einen timestamp zum Date
     * @param l zu konvertierendes Long
     * @return das Datum
     */
    @TypeConverter
    fun toDate(l: Long): Date{
        return Date(l)
    }

    /**
     * konvertiert ein Date zu einem timestamp
     * @param d zu konvertierendes Date
     * @return der timestamp
     */
    @TypeConverter
    fun toLong(d: Date): Long{
        return d.time
    }
}