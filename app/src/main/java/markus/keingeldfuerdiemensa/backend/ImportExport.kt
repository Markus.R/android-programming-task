package markus.keingeldfuerdiemensa.backend

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.os.Environment
import android.os.Looper
import android.os.storage.StorageManager
import android.util.SparseArray
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import java.io.*
import java.lang.Exception
import java.util.*


object ImportExport {

    fun importFromCSVFile(application: Activity) {
        if(checkForStoragePermissions(application)) {
            //PermissionChecker.checkPermission(application, PermissionInfo.)
            doAsync {
                try {
                    val categoryDAO = MoneyDatabase.getInstance(application).categoryDao()
                    val recordDAO = MoneyDatabase.getInstance(application).moneyRecordDao()

                    val root = File(Environment.getExternalStorageDirectory(), "KeinGeldFuerDieMensa")
                    if (root.exists()) {
                        val file = File(root, "backup.csv")
                        if (file.exists()) {

                            // first, delete old table content
                            categoryDAO.deleteAll()
                            recordDAO.deleteAll()

                            val reader = BufferedReader(FileInputStream(file).reader())

                            var curLine = reader.readLine()
                            var tableName: String? = null
                            var skipNextLine = false //skip col declarations
                            var categoryIdMap = SparseArray<Pair<Int, Category>>()

                            while (curLine != null) {
                                if (curLine.startsWith("__")) { //begin of a new table
                                    tableName = curLine.replace("__", "").replace(",", "")
                                    skipNextLine = true
                                } else if (skipNextLine) {
                                    //do nothing, bah
                                    skipNextLine = false
                                } else if (tableName != null) {
                                    val cols = curLine.split(",")
                                    if (tableName == "records") {
                                        println("Record: $cols")
                                        val _id = cols[0].replace("\"", "").toInt() //actually, we won't need this, but it is saved just in case
                                        val creationTimestamp = Date(cols[1].replace("\"", "").toLong())
                                        val amount = cols[2].replace("\"", "").toFloat()
                                        val categoryId = cols[3].replace("\"", "").toInt()
                                        var additionalInfo = ""
                                        if (cols.size > 4) {
                                            additionalInfo = cols[4].replace("\"", "")
                                        }
                                        val record = MoneyRecord(
                                                creationTimestamp = creationTimestamp,
                                                amount = amount,
                                                categoryId = categoryIdMap[categoryId].first,
                                                category = categoryIdMap[categoryId].second,
                                                additionalInfo = additionalInfo
                                        )
                                        recordDAO.insert(record)

                                    } else if (tableName == "categories") {
                                        println("Category: $cols")
                                        val _id = cols[0].replace("\"", "").toInt()
                                        val name = cols[1].replace("\"", "")
                                        var color: Int? = null
                                        if (cols.size > 2) {
                                            color = cols[2].replace("\"", "").toInt()
                                        }
                                        var category: Category
                                        if (color != null) {
                                            category = Category(name = name, color = color)
                                        } else {
                                            category = Category(name = name)
                                        }
                                        val newId = categoryDAO.insert(category)
                                        categoryIdMap.put(_id, Pair(newId.toInt(), category)) //save the old id so we can map the records to new category ids later
                                    }
                                }
                                curLine = reader.readLine()
                            }
                        }
                    } else {
                        Looper.prepare()
                        return@doAsync AppUtils.showQuestionBox(application, null, "Datei nicht gefunden.")
                    }
                } catch (e: Exception) {
                    Looper.prepare()
                    return@doAsync AppUtils.showQuestionBox(application, null, "Fuck.")
                }
            }.execute()
        }
    }

    fun exportToCSVFile(application: Activity){
        doAsync {
            val categoryDAO = MoneyDatabase.getInstance(application).categoryDao()
            val recordDAO = MoneyDatabase.getInstance(application).moneyRecordDao()

            val root = File(Environment.getExternalStorageDirectory(), "KeinGeldFuerDieMensa")
            if (!root.exists()) {
                root.mkdirs() // this will create folder
            }

            val d = Date().toString().replace(":", "-") //do this to open files with excel
            //val file = File(root, "backup_$d.csv")
            val file = File(root, "backup.csv")

            val out = BufferedWriter(FileOutputStream(file).writer())

            println("started exporting...")
            var amountCategories = 0
            var amountRecords = 0

            out.append("sep=,").appendln() //excel fuckery

            //categories first, because they have to be inserted first (foreign keys to records)
            out.append("__categories").appendln()
            out.append("_id,name").appendln()

            categoryDAO.getAllForExport().forEach {
                out.append("\"").append(it._cid.toString()).append("\",")
                        .append("\"").append(it.name).append("\",")
                        .append("\"").append(it.color.toString()).append("\"")
                        .appendln()

                amountCategories++
            }

            out.append("__records").appendln()
            out.append("_id,creationTimestamp,amount,categoryId").appendln()

            recordDAO.getAllForExport().forEach {
                out.append("\"").append(it._mid.toString()).append("\",")
                        .append("\"").append(it.creationTimestamp.time.toString()).append("\",")
                        .append("\"").append(it.amount.toString()).append("\",")
                        .append("\"").append(it.categoryId.toString()).append("\",")
                        .append("\"").append(it.additionalInfo).append("\"")
                        .appendln()
                amountRecords++
            }

            out.flush()
            out.close()

            println("exported $amountCategories categories")
            println("exported $amountRecords records")
            println("finished exporting!")
        }.execute()
    }

    private fun checkForStoragePermissions(application: Activity): Boolean{
        val hasPermission = (ContextCompat.checkSelfPermission(application, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(application, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        if(!hasPermission){
            println("hi ")
            ActivityCompat.requestPermissions(application, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
        return hasPermission
    }
}