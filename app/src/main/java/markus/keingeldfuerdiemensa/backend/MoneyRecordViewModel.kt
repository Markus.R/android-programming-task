package markus.keingeldfuerdiemensa.backend

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.util.*

/**
 * [AndroidViewModel] für [MoneyRecord], welches die Verbindung zwischen dem UI und der Datenbank herstellt
 * @param application die App, in der das ViewModel verwendet wird
 * @property moneyRecords Liste der anzuzeigenden Einträge
 * @property sumOfRecords Summe der Beträge der anzuzeigenden Einträge
 * @property sumIncome Summe der Beträge der anzuzeigenden Einträge im postiven Bereich (Einnahmen)
 * @property sumOutgoing Summe der Beträge der anzuzeigenden Einträge im negativen Bereich (Ausgaben)
 * @constructor fordert eine Liste von Einträgen von der [MoneyDatabase] an
 */
class MoneyRecordViewModel(application: Application) : AndroidViewModel(application) {
    val recordTable: MoneyRecordDAO
    var dateUntilHistory: MutableLiveData<Date> = MutableLiveData() // lowest date when viewing the history
    var dateUntil: MutableLiveData<Date> = MutableLiveData() // lowest date at current calculation period and highest date of history

    //current period
    var moneyRecords : LiveData<List<MoneyRecord>>
        get() = recordTable.getAll(getDateUntil())
    var sumOfRecords: LiveData<Float>get() = recordTable.getSumTotal(getDateUntil())
    var sumIncome: LiveData<Float>get() = recordTable.getSumIncome(getDateUntil())
    var sumOutgoing: LiveData<Float>get() = recordTable.getSumOutgoing(getDateUntil())
    var sumByCategory: LiveData<List<CategorySumTuple>>get() = recordTable.getSumsByCategory(getDateUntil())
    //history period
    var historyMoneyRecords : LiveData<List<MoneyRecord>>get() = recordTable.getAll(getHistoryDateUntil(), getDateUntil())
    var historySumByCategory: LiveData<List<CategorySumTuple>>get() = recordTable.getSumsByCategory(getHistoryDateUntil(), getDateUntil())

    init{
        val currentDate = Date()
        setDateUntil(AppUtils.getStartDate(currentDate, application))
        setHistoryDateUntil(AppUtils.getStartDate(currentDate, application, true))

        recordTable = MoneyDatabase
                .getInstance(getApplication())
                .moneyRecordDao()


        moneyRecords    = recordTable.getAll(getDateUntil())
        sumOfRecords    = recordTable.getSumTotal(getDateUntil())
        sumIncome       = recordTable.getSumIncome(getDateUntil())
        sumOutgoing     = recordTable.getSumOutgoing(getDateUntil())
        sumByCategory   = recordTable.getSumsByCategory(getDateUntil())

        //history
        historyMoneyRecords = recordTable.getAll(getHistoryDateUntil(), getDateUntil())
        historySumByCategory   = recordTable.getSumsByCategory(getHistoryDateUntil(), getDateUntil())
    }

    fun setDateUntil(date: Date){
        dateUntil.value = date
    }

    fun getDateUntil(): Date {
        return dateUntil.value!!
    }

    fun setHistoryDateUntil(date: Date){
        dateUntilHistory.value = date
    }

    fun getHistoryDateUntil(): Date {
        return dateUntilHistory.value!!
    }

    fun getMoneyRecords(itemCount: Int): LiveData<List<MoneyRecord>>{
        return MoneyDatabase
                .getInstance(getApplication())
                .moneyRecordDao().getNewest(itemCount)
    }

    /**
     * ruft die [MoneyDatabase] auf, um ein [MoneyRecord]-Objekt in diese zu speichern
     * @param record der zu speichernde Eintrag
     */
    fun addMoneyRecord(record: MoneyRecord){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .moneyRecordDao()
                    .insert(record)
        }.execute()
    }

    fun updateMoneyRecord(record: MoneyRecord){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .moneyRecordDao()
                    .update(record)
        }.execute()
    }

    /**
     * ruft die [MoneyDatabase] auf, um ein [MoneyRecord]-Objekt aus dieser zu löschen
     * @param record der zu löschende Eintrag
     */
    fun deleteMoneyRecord(record: MoneyRecord){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .moneyRecordDao()
                    .delete(record)
        }.execute()
    }
}