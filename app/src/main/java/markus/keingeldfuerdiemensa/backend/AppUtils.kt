package markus.keingeldfuerdiemensa.backend

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import androidx.appcompat.app.AlertDialog
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.ColorTemplate.rgb
import markus.keingeldfuerdiemensa.R
import markus.keingeldfuerdiemensa.backend.CalculationPeriod.*
import java.text.NumberFormat
import java.util.*


/**
 * Enum für die verschiedenen Berechnungszeiträume
 * @property displayNameResource Resource-ID für den Namen, basierend auf dem [R]-Konzept von Android, um Strings zu übersetzen
 * @property MONTH der laufende Monat ab dem 1.
 * @property WEEK die laufende Woche ab Montag 0 Uhr
 * @property QUARTAL das laufende Quartal
 * @property ALL alle Einträge
 */
enum class CalculationPeriod(val displayNameResource: Int) {
    //keep in sync with arrays.xml
    MONTH(R.string.pref_calculationPeriodOption_month),
    WEEK(R.string.pref_calculationPeriodOption_week),
    QUARTAL(R.string.pref_calculationPeriodOption_quarter),
    ALL(R.string.pref_calculationPeriodOption_all)
}

/**
 * statisches Objekt mit Hilfsfunktionen, die an verschiedenen Stellen der App benutzt werden
 */
object AppUtils {

    val PRESET_COLORS = arrayOf(rgb("f44336"), rgb("E91E63"), rgb("9C27B0"), rgb("673AB7"), rgb("3F51B5"), rgb("2196F3"), rgb("03A9F4"), rgb("00BCD4"), rgb("009688"), rgb("4CAF50"), rgb("8BC34A"), rgb("CDDC39"), rgb("FFEB3B"), rgb("FFC107"), rgb("FF9800"), rgb("FF5722"), rgb("795548"), rgb("9E9E9E"), rgb("607D8B"))
    private val calendar: Calendar = Calendar.getInstance()

    /**
     * Gibt das Feld des Enum zurück, welches dem vom Benutzer ausgewählten Berechnungszeitraum entspricht
     * @param c App-Kontext
     * @return aktuelle [CalculationPeriod]
     */
    fun getCalculationPeriod(c: Context): CalculationPeriod{
        val prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(c)
        val pref = prefs.getString("pref_calculation_period", CalculationPeriod.ALL.name)
        CalculationPeriod.values().iterator().forEach {
            if (pref == it.name) return it
        }
        return CalculationPeriod.ALL //fallback
    }

    fun setCalculationPeriod(c: Context, cp: CalculationPeriod){
        val prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(c)
        prefs.edit().putString("pref_calculation_period", cp.name).apply()
    }

    /**
     * Gibt die Startzeit des Berechnungszeitraumes basierend auf den Benutzer-Einstellungen zurück
     * @param dateNow [Date]-Objekt, welches die aktuelle Zeit beinhaltet
     * @param c App-Kontext
     * @return [Date]-Objekt, welches die Anfangszeit der sichtbaren Einträge beinhaltet (basierend auf den Benutzer-Einstellungen der Berechnungszeit)
     */
    fun getStartDate(dateNow: Date, c: Context, useHistory: Boolean = false): Date {
        calendar.time = dateNow
        when(getCalculationPeriod(c)){
            CalculationPeriod.MONTH -> {
                calendar.set(Calendar.DAY_OF_MONTH, 1)
                if (useHistory) calendar.add(Calendar.MONTH, -1)
            }
            CalculationPeriod.WEEK -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                if (useHistory) calendar.add(Calendar.WEEK_OF_YEAR, -1)
            }
            CalculationPeriod.QUARTAL -> {
                calendar.set(Calendar.DAY_OF_MONTH, 1)
                calendar.add(Calendar.MONTH, - calendar.get(Calendar.MONTH) % 3) // use what's left from the current quarter
                if (useHistory) calendar.add(Calendar.MONTH, -3)
            }
            CalculationPeriod.ALL -> {
                calendar.time = Date(0)
            }

        }
        calendar.set(Calendar.HOUR_OF_DAY, 0) // calculate from the beginning of the day
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar.time
    }

    /**
     * konvertiert eine [Float] zu einem [String], der einen Geldbetrag widerspiegelt - basierend auf [NumberFormat.getCurrencyInstance]
     * @param f [Float], der konvertiert werden soll
     * @return konvertierten [String]
     */
    fun toCurrencyString(f: Float): String{
        return NumberFormat.getCurrencyInstance().format(f)
    }

    /**
     * konvertiert einen [Int] zu einem [String], der einen Geldbetrag widerspiegelt - basierend auf [NumberFormat.getCurrencyInstance]
     * @param i [Int], der konvertiert werden soll
     * @return konvertierten [String]
     */
    fun toCurrencyString(i: Int): String{
        return NumberFormat.getCurrencyInstance().format(i)
    }

    fun showQuestionBox(context: Context, title: String?, question: String, positiveButtonText: String = "Fortfahren",
                        positiveCallback: () -> Unit = {}, negativeCallback: () -> Unit = {}){
        AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(question)

                .setPositiveButton(positiveButtonText) { dialogInterface: DialogInterface?, _ ->
                    positiveCallback()
                    dialogInterface?.dismiss()
                }
                .setNegativeButton("Abbrechen") { dialogInterface: DialogInterface?, _ ->
                    negativeCallback()
                    dialogInterface?.dismiss()
                }
                .create()
                .show()
    }
}


/**
 * Utility-Klasse um Code platzsparend in einem Android [AsyncTask] ausführen zu lassen, Code von https://stackoverflow.com/a/49314329
 * @property handler Funktion, dessen Körper den Code beinhaltet, der ausgeführt werden soll
 * @sample CategoryViewModel.addCategory
 */
class doAsync(private val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {

    /**
     * Funktion beinhaltet den Code, der im AsyncTask ausgeführt werden soll
     * @param params variable Anzahl an Parametern, die in [AsyncTask.execute] übergeben werden können (wird hier aber nicht benötigt)
     */
    override fun doInBackground(vararg params: Void?): Void? {
        handler()
        return null
    }
}
