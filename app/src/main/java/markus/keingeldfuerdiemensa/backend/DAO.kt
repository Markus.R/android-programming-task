package markus.keingeldfuerdiemensa.backend

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

/**
 * Alle Queries, die Room auf die money_record Tabelle anwenden kann. Die Funktionen werden automatisch
 * durch Room implementiert, deren Parameter in die Query eingebunden und Ergebnisse anhand der Rückgabewerte zurückgegeben
 */
@Dao
interface MoneyRecordDAO {

    //gibt alle Einträge zurück
    @Query("SELECT * FROM money_records;")
    fun getAllForExport(): List<MoneyRecord>

    //gibt die neuesten X Einträge zurück
    @Query("SELECT money_records.*, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid ORDER BY creationTimestamp DESC LIMIT :itemCount;")
    fun getNewest(itemCount: Int): LiveData<List<MoneyRecord>>

    /**
     * gibt alle [MoneyRecord]s aus der money_records-Tabelle zurück und sortiert sie nach aufsteigender Erstellungszeit,
     * fügt außerdem durch eine subquery den Name der [Category] hinzu, die zum Eintrag zugeordnet wurde
     * @return Liste der Einträge
     */
    @Query("SELECT money_records.*, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid ORDER BY creationTimestamp DESC;")
    fun getAll(): LiveData<List<MoneyRecord>>

    /**
     * gibt alle [MoneyRecord]s nach einer bestimmten Erstellungszeit aus der money_records-Tabelle zurück und sortiert sie nach aufsteigender Erstellungszeit,
     * fügt außerdem durch eine subquery den Name der [Category] hinzu, die zum Eintrag zugeordnet wurde
     * @param to [Date]-Objekt, bis wohin die Einträge zurückgegeben werden sollen
     * @return Liste der Einträge
     */
    @Query("SELECT money_records.*, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE creationTimestamp > :to ORDER BY creationTimestamp DESC;")
    fun getAll(to: Date): LiveData<List<MoneyRecord>>

    @Query("SELECT money_records.*, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE creationTimestamp < :from AND creationTimestamp > :to ORDER BY creationTimestamp DESC;")
    fun getAll(from: Date, to: Date): LiveData<List<MoneyRecord>>

    /**
     * berechnet die Summe der Beträge der Einträge nach einem gewissen Datum
     * @param to [Date]-Objekt, bis zu welcher Erstellungszeit die Summe der Einträge berechnet werden soll
     * @return [LiveData<Float>], welches die Summe beinhaltet
     */
    @Query("SELECT SUM(amount) FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE creationTimestamp > :to AND categories.isEnabled = 1;")
    fun getSumTotal(to: Date): LiveData<Float>

    /**
     * Berechnet die Summe der Beträge aller positiven Einträge nach einem bestimmten Datum
     * @param to Datum, bis zu welchem die Beträge einberechnet werden sollen
     * @return absoluter Betrag der gewählten Einträge
     */
    @Query("SELECT SUM(amount) FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE amount > 0 AND creationTimestamp > :to AND categories.isEnabled = 1;")
    fun getSumIncome(to: Date): LiveData<Float>

    /**
     * Berechnet die absolute Summe der Beträge aller negativen Einträge nach einem bestimmten Datum
     * @param to Datum, bis zu welchem die Beträge einberechnet werden sollen
     * @return absoluter Betrag der gewählten Einträge
     */
    @Query("SELECT ABS(SUM(amount)) FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE amount < 0 AND creationTimestamp > :to AND categories.isEnabled = 1;")
    fun getSumOutgoing(to: Date): LiveData<Float>

    /**
     * fügt einen neuen [MoneyRecord] in die Tabelle ein
     * @param record der einzufügende Eintrag
     */
    @Insert
    fun insert(record: MoneyRecord): Long

    @Update
    fun update(record: MoneyRecord)

    @Query("SELECT ABS(SUM(amount)) AS amount, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE creationTimestamp > :to AND categories.isEnabled = 1 GROUP BY categories.name;")
    fun getSumsByCategory(to: Date): LiveData<List<CategorySumTuple>>

    @Query("SELECT ABS(SUM(amount)) AS amount, categories.* FROM money_records LEFT JOIN categories ON money_records.categoryId = categories._cid WHERE creationTimestamp > :from AND creationTimestamp < :to AND categories.isEnabled = 1 GROUP BY categories.name;")
    fun getSumsByCategory(from: Date, to: Date): LiveData<List<CategorySumTuple>>

    /**
     * löscht einen bestehenden Eintrag aus der Tabelle
     * @param record der bestehende Eintrag
     */
    @Delete
    fun delete(record: MoneyRecord)

    @Query("DELETE FROM money_records")
    fun deleteAll()
}

/**
 * Alle Queries, die Room auf die categories Tabelle anwenden kann. Die Funktionen werden automatisch
 * durch Room implementiert, deren Parameter in die Query eingebunden und Ergebnisse anhand der Rückgabewerte zurückgegeben
 */
@Dao
interface CategoryDAO {
    /**
     * gibt alle [Category]s aus der categories-Tabelle zurück
     * @return Liste der Kategorien
     */
    @Query("SELECT * FROM categories;")
    fun getAll() : LiveData<List<Category>>

    @Query("SELECT * FROM categories;")
    fun getAllForExport(): List<Category>
    /**
     * fügt eine neue [Category] in die Tabelle ein
     * @param category die einzufügende Kategorie
     */
    @Insert
    fun insert(category: Category): Long

    @Update
    fun update(category: Category)

    /**
     * löscht eine bestehende Kategorie aus der Tabelle
     * @param category die bestehende Kategorie
     */
    @Delete
    fun delete(category: Category)

    @Query("DELETE FROM categories")
    fun deleteAll()
}



data class CategorySumTuple(
        @Embedded val category: Category,
        @ColumnInfo(name = "amount") val sum: Float
)