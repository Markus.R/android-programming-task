package markus.keingeldfuerdiemensa.backend

import android.content.Context
import android.graphics.Color
import android.text.format.DateUtils
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.text.NumberFormat
import java.util.*

/**
 * [MoneyRecord] ist die Kotlin-Repräsentation einer Zeile der [MoneyDatabase] in der money_records -Tabelle
 * @constructor Wird von Room verwendet um Daten aus der [MoneyDatabase] zu repräsentieren oder kann manuell aufgerufen werden um Dummys zu erstellen (diese müssen extra in die DB eingefügt werden, um sie zu speichern)
 * @property _id Eindeutige ID des Eintrages, wird als Primärschlüssel in der Datenbank-Tabelle verwendet und ist unveränderlich
 * @property amount Der Betrag als Float, interpretiert in Euro (Bspw. 1.4f = 1,40€)
 * @property categoryId die eindeutige [Category]-ID, die dem Eintrag zugewiesen werden soll
 * @property categoryName wird automatisch aus dem DAO geladen und beinhaltet den Namen der Kategorie, die unter [categoryId] angegeben ist
 * @property creationTimestamp wird beim erstmaligen Erstellen automatisch generiert und beinhaltet den Timestamp der Erstellungzeit des Eintrages
 */
@Entity(tableName = "money_records",
        foreignKeys = [
                ForeignKey(
                        entity = Category::class,
                        parentColumns = ["_cid"],
                        childColumns = ["categoryId"],
                        onDelete = CASCADE
                )
        ])
data class MoneyRecord(
        @PrimaryKey(autoGenerate = true) var _mid: Int = 0,
        var creationTimestamp: Date = Date(System.currentTimeMillis()),
        var amount: Float,
        var additionalInfo: String = "",
        @Embedded var category: Category,
        var categoryId: Int
){
    /**
     * Gibt zurück, ob der [amount] unter 0 ist.
     * Wird benutzt, um den Typ des Eintrages (Ein- oder Ausgabe) im UI ordentlich zu kennzeichnen.
     * @return true, wenn der [amount] unter 0 (negativ) ist, andernfalls false
     */
    fun isNegative(): Boolean{
        return amount < 0
    }

    /**
     * Nützliche Methode um direkt auf dem Objekt den [amount] in einen besser lesbaren Geldbetrag umzuwandeln und das
     * Vorzeichen zu entfernen (in der Liste wird das Vorzeichen durch ein extra Icon repräsentiert)
     * @return formatierten [String] des [amount]
     */
    fun getAmountAsUnsignedCurrency(): String{
        return NumberFormat.getCurrencyInstance().format(Math.abs(amount))
    }

    fun getCategoryName(): String{
        return category.name
    }

    fun getCategoryNameAndInfo(): String{
        return if(additionalInfo.isNotEmpty()) category.name+" • $additionalInfo" else category.name
    }


    /**
     * Formatiert den [creationTimestamp] in ein lesbares Datum unter Anwendung verschiedener Flags, die angeben, welche
     * Teile der Zeitangabe angezeigt werden
     * @param context Der App-Kontext
     * @return korrekt formatierten [String] des [creationTimestamp] als Zeitangabe
     */
    fun getDateStringFromCreationTimestamp(context: Context): String{
        return DateUtils.formatDateTime(context, creationTimestamp.time,
                DateUtils.FORMAT_SHOW_TIME
                        or DateUtils.FORMAT_SHOW_DATE
                        or DateUtils.FORMAT_SHOW_WEEKDAY
                        or DateUtils.FORMAT_ABBREV_WEEKDAY)
    }
}


/**
 * [Category] ist die Kotlin-Repräsentation einer Zeile der [MoneyDatabase] in der money_records -Tabelle
 * @constructor Wird von Room verwendet um Daten aus der [MoneyDatabase] zu repräsentieren oder kann manuell aufgerufen
 * werden um Dummys zu erstellen (diese müssen extra in die DB eingefügt werden, um sie zu speichern)
 * @property _id Eindeutige ID des Eintrages, wird als Primärschlüssel in der Datenbank-Tabelle verwendet und ist unveränderlich
 * @property name Der sichtbare Name der Kategorie, wird vom Benutzer bei der Erstellung vergeben
 */
@Entity(tableName = "categories")
data class Category(
        @PrimaryKey(autoGenerate = true) var _cid: Int = 0,
        var name: String,
        var color: Int = Color.GRAY,
        var isEnabled: Boolean = true
){
    override fun toString(): String {
        return name
    }
}