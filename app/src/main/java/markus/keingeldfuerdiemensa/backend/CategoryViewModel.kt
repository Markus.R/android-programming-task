package markus.keingeldfuerdiemensa.backend

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * [AndroidViewModel] für [Category], welches die Verbindung zwischen dem UI und der Datenbank herstellt
 * @param application die App, in der das ViewModel verwendet wird
 * @property categories Liste der anzuzeigenden Kategorien
 * @property selected LiveData-Container für die ausgewählte Kategorie
 * @constructor fordert eine Liste von Kategorien von der [MoneyDatabase] an
 */
class CategoryViewModel(application: Application) : AndroidViewModel(application) {
    var categories: LiveData<List<Category>> = MoneyDatabase
            .getInstance(getApplication())
            .categoryDao()
            .getAll()
    var selected : MutableLiveData<Category> = MutableLiveData()

    /**
     * markiert eine [Category] als vom Benutzer ausgewählt (in der Liste angetippt)
     * @param cat Category, die ausgewählt werden soll
     */
    fun setSelected(cat: Category?){
        selected.value = cat
    }

    /**
     * gibt die ausgewählte Kategorie zurück
     * @return ausgewählte Kategorie bzw. null, wenn keine Kategorie ausgewählt wurde
     */
    fun getSelected(): Category? {
        return selected.value
    }

    /**
     * ruft die [MoneyDatabase] auf, um ein [Category]-Objekt in diese zu speichern
     * @param category die zu speichernde Kategorie
     */
    fun addCategory(category: Category){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .categoryDao()
                    .insert(category)
        }.execute()
    }

    //update
    fun updateCategory(category: Category){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .categoryDao()
                    .update(category)
        }.execute()
    }

    /**
     * ruft die [MoneyDatabase] auf, um ein [Category]-Objekt aus dieser zu löschen
     * @param category die zu löschende Kategorie
     */
    fun deleteCategory(category: Category){
        doAsync{
            MoneyDatabase
                    .getInstance(getApplication())
                    .categoryDao()
                    .delete(category)
        }.execute()
    }
}