package markus.keingeldfuerdiemensa

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.utils.ColorTemplate.rgb
import markus.keingeldfuerdiemensa.backend.AppUtils
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentOverviewBinding
import markus.keingeldfuerdiemensa.dialog.CategorySelectDialogFragment

class OverviewFragment : Fragment() {

    val MATERIAL_COLORS = intArrayOf(rgb("#f44336"), rgb("#E91E63"), rgb("#9C27B0"), rgb("#673AB7"), rgb("#3F51B5"), rgb("#2196F3"), rgb("#03A9F4"), rgb("#00BCD4"), rgb("#009688"), rgb("4CAF50"))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentOverviewBinding = DataBindingUtil.inflate(inflater!!, R.layout.fragment_overview, container, false)
        var analyzeFragmentVisible = false
        try {

            var tx = childFragmentManager.beginTransaction()
            var fragment = RecordFragment.newInstance(itemCount = 5)
            tx.replace(R.id.fragmentHolder, fragment)
            tx.commit()

            val categoryViewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)
            val moneyRecordViewModel = ViewModelProviders.of(activity!!).get(MoneyRecordViewModel::class.java)


            val layout = binding.root.findViewById<ViewGroup>(R.id.constraintLayout)
            layout.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

            binding.cardView.setOnClickListener {
                if(analyzeFragmentVisible){
                    binding.analyzeFragmentHolder.removeAllViews()
                }else {
                    tx = childFragmentManager.beginTransaction()
                    val analyzeFragment = DataAnalyzeFragment.newInstance()
                    tx.replace(R.id.analyzeFragmentHolder, analyzeFragment)
                    tx.commit()
                }
                analyzeFragmentVisible = !analyzeFragmentVisible
            }

            //overview card

            moneyRecordViewModel.dateUntil.observe(this, Observer {

                binding.calculationPeriod = AppUtils.getCalculationPeriod(context!!).displayNameResource
                binding.calculationPeriodDesc = "" //AppUtils.getStartDate(Date(), context).toString() + AppUtils.getStartDate(Date(), context, true).toString()

                moneyRecordViewModel.sumOutgoing.observe(this, Observer {
                    if(it is Float){
                        binding.recordSumOutgoing = AppUtils.toCurrencyString(it)
                    }else{
                        binding.recordSumOutgoing = AppUtils.toCurrencyString(0f)
                    }
                })

                moneyRecordViewModel.sumIncome.observe(this, Observer {
                    if(it is Float){
                        binding.recordSumIncome = AppUtils.toCurrencyString(it)
                    }else{
                        binding.recordSumIncome = AppUtils.toCurrencyString(0f)
                    }
                })

                moneyRecordViewModel.sumOfRecords.observe(this, Observer {
                    if(it is Float){
                        binding.recordSum = AppUtils.toCurrencyString(it)
                    }else{
                        binding.recordSum = AppUtils.toCurrencyString(0f)
                    }
                })

                /*binding.btnCategorySelect.setOnClickListener {
                    val ft = childFragmentManager.beginTransaction()
                    CategorySelectDialogFragment.newInstance().show(ft, MainActivity.FRAGMENT_DIALOG_TAG) //create new DialogFragment
                }*/
            })

        } catch (e: Exception) { }

        return binding.root
    }

    companion object {
        fun newInstance() = OverviewFragment()
    }
}
