package markus.keingeldfuerdiemensa

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import markus.keingeldfuerdiemensa.backend.MoneyRecord
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentRecordBinding


/**
 * Adapter für das [RecordFragment], der das [RecyclerView] mit Inhalt füllt und die ausgewählte Kategorie überwacht
 * @property moneyRecords Liste der [MoneyRecord]-Objekte, die im RecyclerView angezeigt werden sollen
 */
class MoneyRecordAdapter(moneyRecords: List<MoneyRecord>, private val viewModel: MoneyRecordViewModel): RecyclerView.Adapter<MoneyRecordAdapter.ViewHolder>(){
    private val TAG = MoneyRecordAdapter::class.java.simpleName

    var moneyRecords = moneyRecords
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    var clickable: Boolean = false
    var baseList: RecordFragment? = null

    /**
     * wird aufgerufen, wenn der ViewHolder erstellt wird und lädt das Layout, was als Binding weiterverwendet werden kann
     * @param parent das übergeordnete View
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentRecordBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
        return ViewHolder(binding)
    }

    /**
     * gibt die Anzahl der anzuzeigenden Zeilen der RecyclerView-Liste zurück
     * @return Größe der Quell-Liste
     */
    override fun getItemCount(): Int {
        return moneyRecords.size
    }

    /**
     * wird aufgerufen, wenn ein neues Element angezeigt werden soll
     * @param holder der ViewHolder des Adapters
     * @param position die Position in der Quell-Liste ([moneyRecords])
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.bind(moneyRecords[position])
    }

    /**
     * Implementierung für den spezifischen ViewHolder von [onCreateViewHolder], welcher die Daten einer einzelnen Zeile der Liste lädt
     * @param binding das Layout-Binding des [RecordFragment] für Interaktion mit dem UI
     */
    inner class ViewHolder(val binding: FragmentRecordBinding) : RecyclerView.ViewHolder(binding.root) {

        /**
         * verbindet ein Eintrags-Objekt mit einer einzelnen Zeile der Liste
         * @param record der Eintrag, die angezeigt werden soll
         */
        fun bind(record: MoneyRecord){
            binding.record = record

            //long click logic for binding
            if (clickable) {
                binding.root.isLongClickable = true
                binding.root.setOnClickListener {
                    baseList?.showEditDialog(record)
                    //Toast.makeText(binding.root.context, "Lang drücken, um den Eintrag zu löschen.", Toast.LENGTH_LONG).show()
                }
                binding.root.setOnLongClickListener {

                    AlertDialog.Builder(binding.root.context)
                            .setMessage("Eintrag wirklich löschen?")
                            .setPositiveButton("Löschen") { dialogInterface: DialogInterface?, _ ->
                                viewModel.deleteMoneyRecord(record)
                                dialogInterface?.dismiss()
                            }
                            .setNegativeButton("Abbrechen") { dialogInterface: DialogInterface?, _ ->
                                dialogInterface?.dismiss()
                            }
                            .create()
                            .show()
                    true
                }
            }
            binding.executePendingBindings()
        }
    }

}