package markus.keingeldfuerdiemensa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_record_list.*
import markus.keingeldfuerdiemensa.backend.MoneyRecord
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentRecordListBinding
import markus.keingeldfuerdiemensa.dialog.EditRecordDialogFragment


/**
 * Fragment um eine Liste von Einträgen und eine Übersicht der Einträge anzuzeigen
 */
class RecordFragment : Fragment() {
    private val TAG = RecordFragment::class.java.simpleName

    private var clickable: Boolean = false
    private var itemCount: Int? = null
    /**
     * wird aufgerufen, wenn das Layout geladen werden soll (und lädt das Layout basierend auf dem [FragmentRecordListBinding]
     * @param inflater der zu benutzende LayoutInflater
     * @param container das View, welches das Fragment beinhalten soll
     * @param savedInstanceState beinhaltet einen Zwischenspeicher des Layouts, falls es bereits zuvor einmal erstellt wurde
     * @return das fertige View
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {

        val binding: FragmentRecordListBinding = DataBindingUtil.inflate(inflater!!, R.layout.fragment_record_list, container, false)
        val moneyRecordViewModel = ViewModelProviders.of(this).get(MoneyRecordViewModel::class.java)
        val viewAdapter = MoneyRecordAdapter(ArrayList(), moneyRecordViewModel).apply {
            this.clickable = this@RecordFragment.clickable
        }

        val layoutManager = LinearLayoutManager(activity)
        binding.list.layoutManager = layoutManager
        binding.list.adapter = viewAdapter
        if (clickable) binding.list.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))


        //fill list
        val moneyRecords: LiveData<List<MoneyRecord>> = if (itemCount == null) moneyRecordViewModel.moneyRecords else moneyRecordViewModel.getMoneyRecords(itemCount!!)
        moneyRecords.observe(this, Observer {
            if(it is List<MoneyRecord>) {
                viewAdapter.moneyRecords = it
                viewAdapter.baseList = this@RecordFragment
                viewSwitcher.displayedChild = if (it.isEmpty()) 0 else 1 //show placeholder text
            }
        })

        return binding.root
    }

    fun showEditDialog(record: MoneyRecord){
        val ft = childFragmentManager.beginTransaction()
        EditRecordDialogFragment.newInstance(record).show(ft, MainActivity.FRAGMENT_DIALOG_TAG) //create new DialogFragment
    }

    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        fun newInstance(clickable: Boolean = false, itemCount: Int? = null): RecordFragment {
            val frag = RecordFragment().apply {
                    this.clickable = clickable
                    this.itemCount = itemCount
            }
            return frag
        }
    }
}
