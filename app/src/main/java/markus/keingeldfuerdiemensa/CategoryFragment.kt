package markus.keingeldfuerdiemensa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentCategoryListBinding
import markus.keingeldfuerdiemensa.dialog.EditCategoryDialogFragment


/**
 * Fragment um eine Liste von Kategorien anzuzeigen
 */
class CategoryFragment : Fragment() {

    private val TAG = CategoryFragment::class.java.simpleName
    private lateinit var viewModel: CategoryViewModel
    private var selectMode: Boolean = false
    /**
     * wird aufgerufen, wenn das Layout geladen werden soll (und lädt das Layout basierend auf dem FragmentCategoryListBinding
     * @param inflater der zu benutzende LayoutInflater
     * @param container das View, welches das Fragment beinhalten soll
     * @param savedInstanceState beinhaltet einen Zwischenspeicher des Layouts, falls es bereits zuvor einmal erstellt wurde
     * @return das fertige View
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentCategoryListBinding = DataBindingUtil.inflate(
                inflater!!, R.layout.fragment_category_list, container, false)

        viewModel = ViewModelProviders.of(activity!!).get(CategoryViewModel::class.java)
        val viewAdapter = CategoryAdapter(ArrayList(), viewModel, selectMode)

        val layoutManager = LinearLayoutManager(activity)
        binding.list.layoutManager = layoutManager
        binding.list.adapter = viewAdapter
        binding.list.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))


        viewModel.categories.observe(this, Observer {
            if(it is List<Category>) {
                viewAdapter.categories = it
                viewAdapter.baseList = this@CategoryFragment
            }
        })
        return binding.root
    }

    fun showEditDialog(category: Category){
        val ft = childFragmentManager.beginTransaction()
        EditCategoryDialogFragment.newInstance(category).show(ft, MainActivity.FRAGMENT_DIALOG_TAG) //create new DialogFragment
    }


    /**
     * Companion Object um eine neue Instanz des Fragments zu erstellen
     */
    companion object {
        //maybe add a bundle of arguments here, so keep the option
        fun newInstance(selectMode: Boolean = false): CategoryFragment {
            val frag = CategoryFragment()
            frag.selectMode = selectMode
            return frag
        }
    }
}
