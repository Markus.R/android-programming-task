package markus.keingeldfuerdiemensa

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import markus.keingeldfuerdiemensa.backend.Category
import markus.keingeldfuerdiemensa.backend.CategoryViewModel
import markus.keingeldfuerdiemensa.databinding.FragmentCategoryBinding

/**
 * Adapter für das [CategoryFragment], der das [RecyclerView] mit Inhalt füllt und die ausgewählte Kategorie überwacht
 * @property categories Liste der [Category]-Objekte, die im RecyclerView angezeigt werden sollen
 * @property viewModel das ViewModel für die Kategorien (um die Auswahl zu speichern)
 */
class CategoryAdapter(categories: List<Category>, private val viewModel: CategoryViewModel, val inSelectMode: Boolean = false):
        RecyclerView.Adapter<CategoryAdapter.ViewHolder>(){

    private val TAG = CategoryAdapter::class.java.simpleName
    private var currentSelectedView: View? = null
    private lateinit var fragment: View
    private var selected: Category? = null
    private var recyclerView: RecyclerView? = null
    var categories = categories
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var baseList: CategoryFragment? = null
    /**
     * wird aufgerufen, wenn der ViewHolder erstellt wird und lädt das Layout, was als Binding weiterverwendet werden kann
     * @param parent das übergeordnete View
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentCategoryBinding.inflate(LayoutInflater.from(parent?.context), parent, false)

        fragment = parent!!
        return ViewHolder(binding)
    }

    /**
     * wird aufgerufen, wenn der Adapter an das [RecyclerView] des übergeordneten Fragment hinzugefügt wird
     * @param recyclerView das RecyclerView
     */
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    /**
     * gibt die Anzahl der anzuzeigenden Zeilen der RecyclerView-Liste zurück
     * @return Größe der Quell-Liste
     */
    override fun getItemCount(): Int {
        return categories.size
    }

    /**
     * wird aufgerufen, wenn ein neues Element angezeigt werden soll
     * @param holder der ViewHolder des Adapters
     * @param position die Position in der Quell-Liste ([categories])
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories[position])
    }

    /**
     * selektiert eine Kategorie in der Liste und speichert diese Selektion im ViewModel, wenn bereits eine andere Kategorie
     * selektiert oder die gleiche Kategorie erneut selektiert wurde, deselektiert es diese.
     * @param c die zu selektierende Kategorie
     * @return true, wenn eine Kategorie selektiert wurde, andernfalls false
     */
    fun setCategorySelected(c: Category): Boolean{ //gets called by viewModel binding in CategoryFragment
        if (recyclerView != null){
            if (currentSelectedView is View){ //deselect it
                currentSelectedView?.setBackgroundColor(0)
            }
            val view = recyclerView?.findViewHolderForAdapterPosition(categories.indexOf(c))?.itemView
            if (currentSelectedView != view) { // only select new view if it changed
                currentSelectedView = view
                viewModel.setSelected(c)
                currentSelectedView?.setBackgroundColor(fragment.resources.getColor(R.color.colorItemSelected))
                return true
            }else{ //deselect it for good
                viewModel.setSelected(null)
                currentSelectedView = null
            }
        }
        return false
    }

    /**
     * Implementierung für den spezifischen ViewHolder von [onCreateViewHolder], welcher die Daten einer einzelnen Zeile der Liste lädt
     * @param binding das Layout-Binding des [CategoryFragment] für Interaktion mit dem UI
     */
    inner class ViewHolder(val binding: FragmentCategoryBinding) : RecyclerView.ViewHolder(binding.root) {

        /**
         * verbindet ein Kategorie-Objekt mit einer einzelnen Zeile der Liste
         * @param category die Kategorie, die angezeigt werden soll
         */
        fun bind(category: Category){
            binding.category = category
            binding.root.isLongClickable = true
            binding.root.setOnClickListener {
                if (inSelectMode) {
                    setCategorySelected(category)
                } else {
                    baseList?.showEditDialog(category)
                }
            }

            if (!inSelectMode) {
                binding.root.setOnLongClickListener {
                    AlertDialog.Builder(binding.root.context)
                            .setTitle("Kategorie wirklich löschen?")
                            .setMessage("Dabei werden auch alle dazugehörigen Einträge gelöscht!")
                            .setPositiveButton("Löschen") { dialogInterface: DialogInterface?, _ ->
                                viewModel.deleteCategory(category)
                                dialogInterface?.dismiss()
                            }
                            .setNegativeButton("Abbrechen") { dialogInterface: DialogInterface?, _ ->
                                dialogInterface?.dismiss()
                            }
                            .create()
                            .show()
                    true
            }
            }
            if (category == selected){
                setCategorySelected(category)
            }
            binding.executePendingBindings()
        }
    }

}