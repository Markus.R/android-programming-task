package markus.keingeldfuerdiemensa

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import markus.keingeldfuerdiemensa.backend.AppUtils
import markus.keingeldfuerdiemensa.backend.CalculationPeriod
import markus.keingeldfuerdiemensa.backend.ImportExport
import markus.keingeldfuerdiemensa.backend.MoneyRecordViewModel
import markus.keingeldfuerdiemensa.dialog.NewCategoryDialogFragment
import markus.keingeldfuerdiemensa.dialog.NewRecordDialogFragment
import markus.keingeldfuerdiemensa.uicomponent.CategoryChipFragment
import java.util.*


/**
 * Hauptklasse der App, die alle wichtigen Layout-Teile lädt, Dialoge öffnet und die Navigationsleiste überwacht
 */
class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private val TAG = "MainActivity"
    private var currentFragmentId = 0 // used to store the resource id for a fragment to prohibit it from loading twice
    private var lastBackPress = 0L // used to determine app-quit logic if user presses back twice in a row

    /**
     * wird aufgerufen, wenn die Activity erstellt wurde
     * @param savedInstanceState ggf. zwischengespeicherte Eigenschaften des Views, wenn es während der Laufzeit erneut erstellt wurde
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //set default user preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)

        //allow vector drawables for older devices
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        fab.setOnClickListener {
            showDialogFragment(currentFragmentId)
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        setFragmentByNavigationItemId(R.id.nav_main)
    }

    /**
     * wird aufgerufen, wenn der Benutzer auf die "Zurück" Taste drückt und hier benutzt, um die Navigationsleiste zu schließen (falls diese offen ist)
     */
    override fun onBackPressed() {
        //if (currentFragmentId != R.id.nav_main) { //switch to main fragment if something other is open
          //  setFragmentByNavigationItemId(R.id.nav_main, true)
         if (System.currentTimeMillis() - lastBackPress > 3000 ) { //handle back press to close logic
            lastBackPress = System.currentTimeMillis()
            Toast.makeText(this, "Erneut drücken, um App zu verlassen", Toast.LENGTH_SHORT).show()
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        val submenu = menu.addSubMenu("Berechnungszeitraum")
        for (strDef in getResources().getStringArray(R.array.calculationPeriodOptionValues)){
            val item = submenu.add(resources.getString(CalculationPeriod.valueOf(strDef).displayNameResource))

            item.setOnMenuItemClickListener {
                AppUtils.setCalculationPeriod(this, CalculationPeriod.valueOf(strDef))
                val moneyRecordViewModel = ViewModelProviders.of(this).get(MoneyRecordViewModel::class.java)
                moneyRecordViewModel.setDateUntil(AppUtils.getStartDate(Date(), this))
                moneyRecordViewModel.setHistoryDateUntil(AppUtils.getStartDate(Date(), this, true))
                return@setOnMenuItemClickListener true
            }

        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
        // action with ID action_refresh was selected
            R.id.action_export -> {
                ImportExport.exportToCSVFile(this)
                Toast.makeText(this, "Backup wird im KeinGeldFuerDieMensa-Ordner auf dem internen Speicher abgelegt...", Toast.LENGTH_LONG).show()
            }
            R.id.action_import -> {
                AppUtils.showQuestionBox(this, "Datenverlust möglich!",
                        "Diese Aktion importiert Daten aus einer Backupdatei und überschreibt bestehende Einträge und Kategorien.", "Importieren", {
                    ImportExport.importFromCSVFile(this)
                })
            }
        }

        return true
    }

    /**
     * wird von NavigationView.OnNavigationItemSelectedListener implementiert und aufgerufen, wenn der Benutzer ein Item in der Navigationsleiste auswählt
     * @param item das Item, welches angeklickt wurde
     * @return true, da in dieser App immer eine Aktion auf einen Klick folgt
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        setFragmentByNavigationItemId(item.itemId)
        return true
    }

    /**
     * ändert das Haupt-Fragment (= den Inhalt) der App basierend auf der Auswahl in der Navigationsleiste und aktualisiert den FloatingActionButton
     * @param id die [R]-Id des Navigationslisten-Items
     * @param customAnimations wenn true, werden Animationen verwendet, die Fragmente nach rechts verschieben
     */
    private fun setFragmentByNavigationItemId(id: Int, customAnimations: Boolean = false) {
        lateinit var newFragment : Fragment
        var fabIcon: Int = -1
        if (id != currentFragmentId) {
            when (id) {
                R.id.nav_main -> {
                    newFragment = OverviewFragment.newInstance()
                    fabIcon = R.drawable.ic_add_black_24dp
                }
                R.id.nav_records -> {
                    newFragment = RecordFragment.newInstance(true)
                    fabIcon = R.drawable.ic_add_black_24dp
                }
                R.id.nav_categories -> {
                    newFragment = CategoryFragment.newInstance()
                    fabIcon = R.drawable.ic_playlist_add_black_24dp
                }
                /*R.id.nav_settings -> {
                    newFragment = AppSettingsFragment.newInstance()
                }*/
            }
            try {
                val tx = supportFragmentManager.beginTransaction()
                tx.setCustomAnimations(R.anim.enter, R.anim.exit)
                tx.replace(R.id.fragmentHolder, newFragment, FRAGMENT_MAIN_TAG)
                tx.commit()
                //nav_view.setCheckedItem(id)
                if (fabIcon != -1){
                    fab.show()
                    fab.setImageResource(fabIcon)
                }else{
                    fab.hide()
                }
                currentFragmentId = id
            } catch (e: Exception) {
                Log.e(TAG, "Error changing fragment from navigationView:")
            }
        }
    }

    /**
     * öffnet ein Dialogfragment basierend auf dem derzeitigen Hauptfragment und somit der Bedeutung des FloatingActionButton
     * @param currentMainFragment die [R]-Id des derzeitigen Hauptfragments
     */
    private fun showDialogFragment(currentMainFragment : Int?){
        val ft = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag(FRAGMENT_DIALOG_TAG)
        if (prev != null){
            Log.d(TAG, "dialog still open, possible leak?")
            ft.remove(prev) // remove old fragment dialog if there is any
        }
        ft.addToBackStack(null)

        when (currentMainFragment){
            R.id.nav_main -> NewRecordDialogFragment.newInstance().show(ft, FRAGMENT_DIALOG_TAG) //create new DialogFragment
            R.id.nav_records -> NewRecordDialogFragment.newInstance().show(ft, FRAGMENT_DIALOG_TAG) //create new DialogFragment
            R.id.nav_categories -> NewCategoryDialogFragment.newInstance().show(ft, FRAGMENT_DIALOG_TAG) //create new DialogFragment
        }

    }

    companion object {
        val FRAGMENT_DIALOG_TAG = "dialog"
        val FRAGMENT_MAIN_TAG = "main-view"
    }
}
